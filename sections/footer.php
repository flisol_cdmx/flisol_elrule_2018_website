    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="site-info">¡El FLISoL es de todos para todos! <br> <!-- Plantilla por <a href="http://technextit.com">Technext Limited</a> --></p>
                    <ul class="social-block">
                        <li><a href="https://twitter.com/hashtag/flisolrule?f=tweets&vertical=default&src=hash" target="new"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/groups/195413897714708/" target="new"><i class="ion-social-facebook"></i></a></li>
                        <!-- <li><a href=""><i class="ion-social-linkedin-outline"></i></a></li> -->
                        <!-- <li><a href=""><i class="ion-social-googleplus"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </footer>