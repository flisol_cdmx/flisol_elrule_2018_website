<section id="hardware_workshops" class="section schedule">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">Talleres de Hardware</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="10:30"></time> - <time datetime="11:00"></time>
                        </div>
                        <h3 class="h3-sp">Taller Básico de Tarjetas Electrónicas con KiCad</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="11:00"></time> - <time datetime="11:30"></time>
                        </div>
                        <h3 class="h3-sp">Arduino para Novatos</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="16:30"></time> - <time datetime="17:30"></time>
                        </div>
                        <h3 class="h3-sp">Road to DevOps: de SysAdmin a la Nube - Automatización 101</h3>
                        <p></p>
                        
                    </div>
                </div>
            </div>
    </section>