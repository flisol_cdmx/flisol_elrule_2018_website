    <nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
        <div class="container">
            <div class="navbar-header">

                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="/">
                        
                        <!-- logo image  -->
                        <img src="assets/images/flisol_logo.png" alt="Logo">

                        FLISoL El Rule 2018
                    </a>
                </div>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div><!-- /.navbar-header -->

            <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">

                    <!-- navigation menu -->
                    <!-- <li class="active"><a data-scroll href="#about">Acerca de</a></li> -->
                    <!-- <li><a data-scroll href="#speakers">Oradores</a></li> -->              
                    <li><a data-scroll href="#schedule">Agenda</a></li>
                    <li><a data-scroll href="#hardware_workshops">Talleres</a></li>                  
                    <li><a data-scroll href="#partner">Patrocinadores</a></li>                  
                    <!-- <li><a data-scroll href="#">Sponsorship</a></li> -->
                    <!-- <li><a data-scroll href="#faq">FAQ</a></li> -->
                    <!-- <li><a data-scroll href="#photos">Fotos</a></li> -->
                    <li><a data-scroll href="#location">Donde</a></li>
                
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>