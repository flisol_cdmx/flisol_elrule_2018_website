<section id="software_workshops" class="section schedule">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">Talleres de Software</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="10:30"></time> - <time datetime="11:00"></time>
                        </div>
                        <h3 class="h3-sp">Wordpress, más que Blogs</h3>
                        <p>Instructor: Rodrigo Patiño</p>
                        <a class="btn btn-white" href="#" data-toggle="modal" data-target="#modalWordpressMasqueBlogs">Más info</a>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="modalWordpressMasqueBlogs" role="dialog">
                        <div class="modal-dialog">
    
                            <!-- Contenido de la ventana modal-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Wordpress, más que Blogs</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>
                                    Instructor: Rodrigo Patiño
                                    </h4>
                                    <p>Objetivo: Crear un template de Wordpress que sirva como landing page para un portafolio.</p>
                                    <p>¿Qué aprenderé en este taller?</p>
                                    <ul>
                                    <li>¿Qué es Wordpress?</li>
                                    <li>¿Quién usa wordpress?</li>
                                    <li>Ventajas de wp</li>
                                    <li>Instalar wp en un servidor local</li>
                                    <li>Archivos básicos de un template</li>
                                    <li>Mostrar datos con loop básico</li>
                                    <li>Crear un custom loop para datos específicos</li>
                                    <li>Crear campos personalizados</li>
                                    </ul>
                                    <p>
                                    Requisitos:
                                    </p>
                                    <ul>
                                    <li>Servidor local (XAMP, MAMP)</li>
                                    <li>Conocimientos básicos de HTML5 y CSS3</li>
                                    <li>Conocimientos de bootstrap (no indispensable)</li>
                                    <li>Editor de texto (Atom, Sublime, Brackets, etc)</li>
                                    </ul>
                                    <h4>
                                    Bio
                                    </h4>
                                    <p>Rodrigo Patiño tiene 25 años y comenzó a aprender HTML a los 15 años. Durante estos 10 años ha trabajado con PHP, Javascript, C++ y sistemas operativos GNU/Linux. Actualmente estudia ingeniería en computación en el IPN y trabaja como desarrollador/sysadmin en una agencia de desarrollo.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    <!-- Termina Modal -->
                </div>

                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="11:00"></time> - <time datetime="11:30"></time>
                        </div>
                        <h3 class="h3-sp">Uso y Manejo de GPG</h3>
                        <p>Instructor: José Emilio Sánchez Juárez</p>
                        <a class="btn btn-white" href="#" data-toggle="modal" data-target="#modalUsoManejoDeGPG">Más info</a>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="modalUsoManejoDeGPG" role="dialog">
                        <div class="modal-dialog">
    
                            <!-- Contenido de la ventana modal-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">USo y Manejo de GPG</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>
                                    Instructor: José Emilio Sánchez Juárez
                                    </h4>
                                    <p>Objetivo: Realizar un taller en el cual el asistente aprende el uso de GPG par cifrar, firmar y verificar la autenticidad del emisor, a su vez el asistente conocerá la importancia de darle importancia a su información para preservar su privacidad.</p>
                                    <h4>Descripcción</h4>
                                    <p>
                                    Se realizará este taller para que el asistente implemente el uso de GPG (Cifrado Asimétrico), GNU Privacy Guard es un derivado libre de PGP y su utilidad es cifrar y firmar digitalmente, siendo además multiplataforma, el asistente genera una clave y distinguirá entre una clave pública a una clave privada, aprenderá a cifrar, firmar y verificar la autenticidad de los mensajes recibidos por diferentes emisores, como también enviar mensajes con las características antes mencionadas, una vez realizada su clave GPG si el asistente desea compartir su clave se explicará como compartir su clave pública en un servidor de claves, si el tiempo transcurrido propicia a explicar como crear su anillo de confianza de claves GPG para un uso en el ambito laboral o personal se explicará a detalle.
                                    </p>
                                    <h4>
                                    Conocimientos previos
                                    </h4>
                                    <ul>
                                    <li>Conocimientos básicos en S.O. GNU/Linux</li>
                                    <li>Uso de terminal</li>
                                    </ul>
                                    <h4>
                                    Requerimientos
                                    </h4>
                                    <p>Software</p>
                                    <ul>
                                    <li>S.O GNU/Linux se recomienda cualquier distribución de la familia Debian</li>
                                    <li>GPG instalado</li>
                                    <li>Editor de texto plano de su preferencia, vi/vim, Emacs, Sublime Text, Lealpad, etc</li>
                                    </ul>
                                    <p>Hardware</p>
                                    <ul>
                                    <li>S.O GNU/Linux se recomienda cualquier distribución de la familia Debian, ya sea instalado (bare install) o virtualizado.</li>
                                    </ul>
                                    <p>Red</p>
                                    <ul>
                                    <li>Suficiente para poder importar a un servidor de claves</li>
                                    </ul>
                                    <h4>
                                    Bio
                                    </h4>
                                    <p>Institución de procedencia: UNAM FES Acatlán</p>
                                    <p>Wiggi</p>
                                    <ul>
                                    <li>Desarrollo y ejecución de análisis de vulnerabilidades web</li>
                                    <li>Destrucción de datos (borrado seguro) digital y seguro</li>
                                    <li>Administración de servidores Linux</li>
                                    </ul>
                                    <p>Cursos</p>
                                    <ul>
                                    <li>Creación de contenido y cursos de Linux para FES Acatlán. FES Acatlán - 2016 - 2018</li>
                                    <li>Manejo e Implementación de GPG - FES Acatlán - 2016</li>
                                    <li>Desarrollo de Software Seguro - Jefatura de Matemáticas Aplicadas y Computación (M@C) FES Acatlán - 2017</li>
                                    <li>Desarrollo de material multimedia con RevealJS. FES Acatlán - 2017</li>
                                    <li>Desarrollo de material multimedia con TeX. FES Acatlán - 2017</li>
                                    <li>Procesamiento de Lenguaje Natural en Python - GIL (Grupo de Ingeniería Lingüistica) UNAM - 2017</li>
                                    </ul>
                                    <p>Proyectos</p>
                                    <ul>
                                    <li>Obtención del 2do lugar del concurso de Programación de la Semana de M@C Matemáticas Aplicadas y Computación - 2016</li>
                                    <li>Integrante del equipo de traducción del S.O. Tails</li>
                                    <li>Organizador del FLISOL de los años 2016 - 2018 en la FES Acatlán</li>
                                    <li>Generación de CIDWA (Club de Investigación y Desarrollo Web Acatlán)</li>
                                    <li>Integrante de LIDSOL (Laboratorio de Investigación y Desarrollo de Software Libre). C.U. - UNAM</li>
                                    <li>Integrante de (SecureTeam) ESCOM</li>
                                    <li>Integrante de LASC (Laboratorio de Seguridad en Computo) FES Acatlán</li>
                                    </ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Termina Modal -->
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="16:30"></time> - <time datetime="17:30"></time>
                        </div>
                        <h3 class="h3-sp">La Historia de Git</h3>
                        <p></p>
                    </div>
                </div>
            </div>
    </section>