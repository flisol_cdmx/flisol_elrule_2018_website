<section id="schedule" class="section schedule">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">Agenda, horarios por definir</h3>
                </div>
            </div>
            <div class="row row-sp">
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="10:30">10:30 am</time> - <time datetime="11:00">11:00 am</time>
                        </div>
                        <h3 class="h3-sp">Registro y Recepción</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="11:00"></time> - <time datetime="11:30"></time>
                        </div>
                        <h3 class="h3-sp">Qué es el Software Libre</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="16:30"></time> - <time datetime="17:30"></time>
                        </div>
                        <h3 class="h3-sp">Conociendo a Linux desde sus inicios hasta la actualidad</h3>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row row-sp">
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="10:30"></time> - <time datetime="11:00"></time>
                        </div>
                        <h3 class="h3-sp">La influencia de la filosofía del SL en la industria de la Seguridad Informática</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="11:00"></time> - <time datetime="11:30"></time>
                        </div>
                        <h3 class="h3-sp">Memorias de un Honeypot Linux</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="16:30"></time> - <time datetime="17:30"></time>
                        </div>
                        <h3 class="h3-sp">También hay Hardware Libre</h3>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row row-sp">
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="10:30"></time> - <time datetime="11:00"></time>
                        </div>
                        <h3 class="h3-sp">Qué es el software Libre</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="11:00"></time> - <time datetime="11:30"></time>
                        </div>
                        <h3 class="h3-sp">Creando servicios para la industria sin software privativo</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="16:30"></time> - <time datetime="17:30"></time>
                        </div>
                        <h3 class="h3-sp">Hackeando tu mente</h3>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row row-sp">
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="10:30"></time> - <time datetime="11:00"></time>
                        </div>
                        <h3 class="h3-sp">De matrices y otras metáforas del Machine Learning</h3>
                        <p></p>
                        
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="11:00"></time> - <time datetime="11:30"></time>
                        </div>
                        <h3 class="h3-sp">Rolling Release, el futuro de las distribuciones GNU/Linux a.k.a Tumbleweed</h3>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="schedule-box">
                        <div class="time">
                            <time datetime="16:30"></time> - <time datetime="17:30"></time>
                        </div>
                        <h3 class="h3-sp">Compartiendo código entre aplicaciones Web y Mobile con Nativescript y Angular</h3>
                        <p></p>
                    </div>
                </div>
            </div>
            
    </section>