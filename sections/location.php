    <section id="location" class="section location">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h3 class="section-title">Ubicación del Evento</h3>
                    <address>
                        <p>Centro Cultural <br> El Rule<br> Eje Central # 6,2º Piso, Col. Centro, CDMX</p>
                    </address>
                </div>
                <div class="col-sm-9">
                    <iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJLWooTiv50YURQiETwuQon70&key=AIzaSyB7E5laAAYcFZclL7KRjW82f6kurNXKJfs&language=es" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>