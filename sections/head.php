<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Ritalin_P" />
    <meta name="twitter:creator" content="@hypertaboo" />
    <meta property="og:url" content="http://flisolrule.com" />
    <meta property="og:title" content="FLISoL El Rule" />
    <meta property="og:description" content="Festival Latinoamericano de Instalación de Software Libre 2018 en el Centro Cultural El Rule. 21 de abril de 2018. Eje Central Lázaro Cárdenas 6, Centro Histórico, Centro, 06000, Ciudad de México" />
    <meta property="og:image" content="http://flisolrule.com/assets/images/flisol_elrule.png" />

    <title>FLISoL El Rule 2018</title>

    <!-- css -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body data-spy="scroll" data-target="#site-nav">